//
//  ServerManager.swift
//  BalinaSoft-test
//
//  Created by Mac on 04.02.2019.
//  Copyright © 2019 Koldushko Aleksei. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class ServerManager {
    
    static var shared = ServerManager()
    private init() {}
    
    
    func checkDomen(domen: String ,_ onCompletion: @escaping (Bool) -> Void) {
        
        let urlToString = URL(string: "https://\(domen)/")
        
        guard let url = urlToString else {return}
        
        Alamofire.request(url)
            .responseString { response in
                print("Success: \(response.result.isSuccess)")
                var statusCode = response.response?.statusCode
                if let error = response.result.error as? AFError {
                    
                    statusCode = error._code 
                }
                print(statusCode as Any)
                if statusCode == 200 {
                    onCompletion(true)
                } else {
                    onCompletion(false)
                }
        }
        
    }
}

