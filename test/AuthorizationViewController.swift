//
//  AuthorizationViewController.swift
//  BalinaSoft-test
//
//  Created by Mac on 04.02.2019.
//  Copyright © 2019 Koldushko Aleksei. All rights reserved.
//

import UIKit
import SearchTextField
import Alamofire

class AuthorizationViewController: UIViewController, UITextFieldDelegate {
    
    // MARK: - PROPERTIES
    
    var gester = UITapGestureRecognizer()
    var swipGester = UISwipeGestureRecognizer()
    
    var gradientLayer: CAGradientLayer! {
        didSet {
            gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
            gradientLayer.endPoint = CGPoint(x: 0.6, y: 0.5)
            let leftCollor = UIColor(red: 10/255, green: 196/255, blue: 186/255, alpha: 1).cgColor
            let rigntColor = UIColor(red: 43/255, green: 218/255, blue: 142/255, alpha: 1).cgColor
            gradientLayer.colors = [leftCollor, rigntColor]
        }
    }
    // MARK: - OUTLETS
    
    @IBOutlet weak var enterEmailTextField: SearchTextField!
    
    @IBOutlet weak var registerButton: UIButton!
    
    @IBOutlet weak var enterPasswordTextField: UITextField!
    
    @IBOutlet weak var seePasswordButton: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var emailTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var logoTopConstraint: NSLayoutConstraint!
    
    // MARK: - ACTIONS
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        signIn()
        passwordChek()
    }
    
    @IBAction func seePasswordButtonClicked(_ sender: Any) {
        enterPasswordTextField.isSecureTextEntry.toggle()
    }
    
    // MARK: - LIFE CYCLE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDown(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardUP(notification:)), name: UIWindow.keyboardWillShowNotification, object: nil)
        
        getCurrentDevice()
        setRegisterButton()
        autocompition()
        gestureRecognizer()
        textFieldStyle()
        statusBar()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        gradientLayer.frame = CGRect.init(x: 0, y: 0, width: self.registerButton.bounds.width, height: self.registerButton.bounds.height)
        gradientLayer.cornerRadius = 7
    }
    
    // MARK: - FUNCTIONS
    
    func getCurrentDevice() {
        emailTopConstraint.constant = UIScreen.main.nativeBounds.height/5.68
        logoTopConstraint.constant = UIScreen.main.nativeBounds.height/22.75
    }
    
    func setRegisterButton() {
        gradientLayer = CAGradientLayer()
        registerButton.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func gestureRecognizer() {
        gester.numberOfTapsRequired = 1
        gester.addTarget(self, action: #selector(hiddeKeyboardInToch))
        self.scrollView.addGestureRecognizer(gester)
        
        swipGester.direction = UISwipeGestureRecognizer.Direction.down
        swipGester.addTarget(self, action: #selector(hiddeKeyboardInToch))
        self.view.addGestureRecognizer(swipGester)
    }
    
    func textFieldStyle() {
        enterEmailTextField.attributedPlaceholder = NSAttributedString(string: "Введите Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        enterPasswordTextField.attributedPlaceholder = NSAttributedString(string: "Введите пароль", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.5)])
        
    }
    
    func autocompition() {
        enterEmailTextField.inlineMode = true
        enterEmailTextField.startFilteringAfter = "@"
        enterEmailTextField.startSuggestingInmediately = true
        enterEmailTextField.filterStrings(["gmail.com", "mail.ru", "tut.by", "yahoo.com", "rambler.ru", "yandex.ru", "myplace.com", "outlook.live.com", "yandex.by", "whitepages.com", "ukr.net", "extmedia.by"])
    }
    
    func statusBar() {
        UIApplication.shared.statusBarStyle = .lightContent
    }
    
    func gradientButton(cornerR: Double) {
        let leftCollor = UIColor(red: 10/255, green: 196/255, blue: 186/255, alpha: 1).cgColor
        let rigntColor = UIColor(red: 43/255, green: 218/255, blue: 142/255, alpha: 1).cgColor
        gradientLayer = CAGradientLayer()
        gradientLayer.colors = [leftCollor, rigntColor]
        gradientLayer.frame = CGRect.init(x: 0, y: 0, width: self.registerButton.bounds.width, height: self.registerButton.bounds.height)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 0.6, y: 0.5)
        gradientLayer.cornerRadius = CGFloat(cornerR)
        self.registerButton.layer.addSublayer(gradientLayer)
        self.registerButton.layer.cornerRadius = CGFloat(cornerR)
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message , preferredStyle: .alert )
        let alertAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(alertAction)
        self.present(alert, animated: false, completion: nil)
    }
    
    func signIn() {
        
        if let unwrapEmail = enterEmailTextField.text,
            let unwrapPassword = enterPasswordTextField.text {
            
            if !unwrapEmail.isEmpty,
                !unwrapPassword.isEmpty {
                
                if unwrapEmail.isValidEmail() {
                    print("Email правильный")
                } else {
                    showAlert(title: "Ошибка!", message: "Не соответствующий формат Email.")
                }
                
                let array = unwrapEmail.components(separatedBy: ["@"])
                if let arrayDomen = array.last{
                    let domen = arrayDomen
                    ServerManager.shared.checkDomen(domen: domen) { (check) in
                        switch check {
                        case true:
                            self.showAlert(title: "УСПЕШНО", message: "")
                        case false:
                            self.showAlert(title: "Ошибка!", message: "Несуществующий Email.")
                            
                        }
                    }
                }
            } else {
                showAlert(title: "Ошибка!", message: "Заполните все поля.")
            }
        }
    }
    
    
    func passwordChek() {
        if let enterPswdTF = enterPasswordTextField.text?.count {
            let enterPSWD = enterPswdTF
            if enterPSWD < 8 {
                showAlert(title: "Короткий пароль!", message: "Ваш пороль должен состоять не менее, чем из 8 символов.")
            } else if enterPSWD > 24 {
                showAlert(title: "Длинный пароль!", message: "Ваш пороль должен состоять не более, чем из 24 символов.")
            } else {
                print("password correct")
            }
        }
    }
    
    // MARK: - OBJC FUNCTIONS
    
    @objc func keyboardDown(notification: Notification) {
        scrollView.contentOffset = .zero
    }
    
    @objc func keyboardUP(notification: Notification) {
        guard let keyboardFrameValue = notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue else {
            return
        }
        
        let keyboardFrame = view.convert(keyboardFrameValue.cgRectValue, from: nil)
        scrollView.contentOffset = CGPoint(x:0, y: keyboardFrame.height/4 + 50 )
    }
    
    @objc func hiddeKeyboardInToch (gester : UIGestureRecognizer) {
        scrollView.contentOffset = .zero
        enterEmailTextField.resignFirstResponder()
        enterPasswordTextField.resignFirstResponder()
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
    }
}



extension String {
    func isValidEmail() -> Bool {
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: count)) != nil
    }
}
